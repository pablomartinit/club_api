<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityGallery extends Model
{
    //
    public function  activity(){
    	return $this->belongsTo(Activity::class);
    }
}
