<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentMatch extends Model
{
	protected $fillable = ["fechahora","lugar","estado","goles","goles_rival","tournament_fixture_date_id","category_id","club_rival_id","estado"];
    public function club(){
    	return $this->belongsTo(Club::class);
    }

    public function category(){
    	return $this->belongsTo(Category::class);
    }

    /* 
    *	SCOPE - Fecha ocupada
    */
    public function scopeOcupado_fecha($query,$datos){
    	$query->where("fechahora",$datos);
    }

}
