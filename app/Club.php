<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public function actividades(){
    	return $this->hasMany(Activity::class);
    }
}
