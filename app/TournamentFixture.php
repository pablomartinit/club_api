<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentFixture extends Model
{

	protected $fillable = ["titulo","tournament_id"];
    public function tournament(){
    	return $this->belongsTo(Tournament::class);
    }

    public function date(){
    	return $this->hasMany(TournamentFixtureDate::class);
    }
}
