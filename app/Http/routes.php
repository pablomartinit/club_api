<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Clubes
|--------------------------------------------------------------------------
|
|
*/
// GET VISTA INDEX
Route::get("clubes",["as"=>"clubs.index","uses"=>"ClubController@getIndex"]);
Route::get("clubes/index","ClubController@index");
Route::get("clubes/crear",["as"=>"clubs.create","uses"=>"ClubController@create"]);
Route::put("clubes/{id}",["as"=>"clubs.update","uses"=>"ClubController@update"]);
Route::get("clubes/{id}",["as"=>"clubs.show","uses"=>"ClubController@show"]);
Route::delete("clubes/{id}",["as"=>"clubs.destroy","uses"=>"ClubController@destroy"]);
Route::get("clubes/{id}/editar",["as"=>"clubs.edit","uses"=>"ClubController@edit"]);
Route::post("clubes",["as"=>"clubs.store","uses"=>"ClubController@store"]);

/*
|--------------------------------------------------------------------------
| Actividades
|--------------------------------------------------------------------------
|
|
*/
// GET VISTA INDEX
Route::get("actividades",["as"=>"activities.index","uses"=>"ActivityController@getIndex"]);
Route::get("actividades/index","ActivityController@index");
Route::get("actividades/crear",["as"=>"activities.create","uses"=>"ActivityController@create"]);
Route::put("actividades/{id}",["as"=>"activities.update","uses"=>"ActivityController@update"]);
Route::get("actividades/{id}",["as"=>"activities.show","uses"=>"ActivityController@show"]);
Route::delete("actividades/{id}",["as"=>"activities.destroy","uses"=>"ActivityController@destroy"]);
Route::get("actividades/{id}/editar",["as"=>"activities.edit","uses"=>"ActivityController@edit"]);
Route::post("actividades",["as"=>"activities.store","uses"=>"ActivityController@store"]);

/*
|--------------------------------------------------------------------------
| Galeria de Actividades
|--------------------------------------------------------------------------
|
|
*/
// GET VISTA INDEX
Route::get("actividad/galeria",["as"=>"activity_galleries.index","uses"=>"ActivityGalleryController@getIndex"]);
Route::get("actividad/galeria/index","ActivityGallery@index");
Route::get("actividad/galeria/crear",["as"=>"activity_galleries.create","uses"=>"ActivityGalleryController@create"]);
Route::put("actividad/galeria/{id}",["as"=>"activity_galleries.update","uses"=>"ActivityGalleryController@update"]);
Route::get("actividad/galeria/{id}",["as"=>"activity_galleries.show","uses"=>"ActivityGalleryController@show"]);
Route::delete("actividad/galeria/{id}",["as"=>"activity_galleries.destroy","uses"=>"ActivityGalleryController@destroy"]);
Route::get("actividad/galeria/{id}/editar",["as"=>"activity_galleries.edit","uses"=>"ActivityGalleryController@edit"]);
Route::post("actividad/galeria",["as"=>"activity_galleries.store","uses"=>"ActivityGalleryController@store"]);


/*
|--------------------------------------------------------------------------
| Categorias
|--------------------------------------------------------------------------
|
|
*/
Route::get("categorias/crear",["as"=>"categories.create","uses"=>"CategoryController@create"]);
Route::get("categorias",["as"=>"categories.index","uses"=>"CategoryController@index"]);
Route::put("categorias/{id}",["as"=>"categories.update","uses"=>"CategoryController@update"]);
Route::get("categorias/{id}",["as"=>"categories.show","uses"=>"CategoryController@show"]);
Route::delete("categorias/{id}",["as"=>"categories.destroy","uses"=>"CategoryController@destroy"]);
Route::get("categorias/{id}/editar",["as"=>"categories.edit","uses"=>"CategoryController@edit"]);
Route::post("categorias",["as"=>"categories.store","uses"=>"CategoryController@store"]);

/*
|--------------------------------------------------------------------------
| Torneos
|--------------------------------------------------------------------------
|
|
*/
Route::get("torneos/crear",["as"=>"tournaments.create","uses"=>"TournamentController@create"]);
Route::get("torneos",["as"=>"tournaments.index","uses"=>"TournamentController@index"]);
Route::put("torneos/{id}",["as"=>"tournaments.update","uses"=>"TournamentController@update"]);
Route::get("torneos/{id}",["as"=>"tournaments.show","uses"=>"TournamentController@show"]);
Route::delete("torneos/{id}",["as"=>"tournaments.destroy","uses"=>"TournamentController@destroy"]);
Route::get("torneos/{id}/editar",["as"=>"tournaments.edit","uses"=>"TournamentController@edit"]);
Route::post("torneos",["as"=>"tournaments.store","uses"=>"TournamentController@store"]);

Route::get("fixture/completo",["as"=>"tournament_fixtures.completo","uses"=>"TournamentFixtureController@getForm"]);
Route::post("fixture/completo","TournamentFixtureController@postForm");
Route::get("fixture/actividad/{id}", "TournamentFixtureController@actividad" );


Route::get("fixture/crear",["as"=>"tournament_fixtures.create","uses"=>"TournamentFixtureController@create"]);
Route::get("fixture",["as"=>"tournament_fixtures.index","uses"=>"TournamentFixtureController@index"]);
Route::put("fixture/{id}",["as"=>"tournament_fixtures.update","uses"=>"TournamentFixtureController@update"]);
Route::get("fixture/{id}",["as"=>"tournament_fixtures.show","uses"=>"TournamentFixtureController@show"]);
Route::delete("fixture/{id}",["as"=>"tournament_fixtures.destroy","uses"=>"TournamentFixtureController@destroy"]);
Route::get("fixture/{id}/editar",["as"=>"tournament_fixtures.edit","uses"=>"TournamentFixtureController@edit"]);
Route::post("fixture",["as"=>"tournament_fixtures.store","uses"=>"TournamentFixtureController@store"]);

Route::get("jornadas/crear",["as"=>"tournament_fixture_dates.create","uses"=>"TournamentFixtureDateController@create"]);
Route::get("jornadas",["as"=>"tournament_fixture_dates.index","uses"=>"TournamentFixtureDateController@index"]);
Route::put("jornadas/{id}",["as"=>"tournament_fixture_dates.update","uses"=>"TournamentFixtureDateController@update"]);
Route::get("jornadas/{id}",["as"=>"tournament_fixture_dates.show","uses"=>"TournamentFixtureDateController@show"]);
Route::delete("jornadas/{id}",["as"=>"tournament_fixture_dates.destroy","uses"=>"TournamentFixtureDateController@destroy"]);
Route::get("jornadas/{id}/editar",["as"=>"tournament_fixture_dates.edit","uses"=>"TournamentFixtureDateController@edit"]);
Route::post("jornadas",["as"=>"tournament_fixture_dates.store","uses"=>"TournamentFixtureDateController@store"]);

Route::get("partidos/crear",["as"=>"tournament_matches.create","uses"=>"TournamentMatchController@create"]);
Route::get("partidos",["as"=>"tournament_matches.index","uses"=>"TournamentMatchController@index"]);
Route::put("partidos/{id}",["as"=>"tournament_matches.update","uses"=>"TournamentMatchController@update"]);
Route::get("partidos/{id}",["as"=>"tournament_matches.show","uses"=>"TournamentMatchController@show"]);
Route::delete("partidos/{id}",["as"=>"tournament_matches.destroy","uses"=>"TournamentMatchController@destroy"]);
Route::get("partidos/{id}/editar",["as"=>"tournament_matches.edit","uses"=>"TournamentMatchController@edit"]);
Route::post("partidos",["as"=>"tournament_matches.store","uses"=>"TournamentMatchController@store"]);


/*
|--------------------------------------------------------------------------
| Noticias
|--------------------------------------------------------------------------
|
|
*/
// GET VISTA INDEX
Route::get("noticias",["as"=>"notices.index","uses"=>"NoticeController@index"]);
// Route::get("noticias",["as"=>"notices.index","uses"=>"NoticeController@getIndex"]);
// Route::get("noticias/index","NoticeController@index");
Route::get("noticias/crear",["as"=>"notices.create","uses"=>"NoticeController@create"]);
Route::put("noticias/{id}",["as"=>"notices.update","uses"=>"NoticeController@update"]);
Route::get("noticias/{id}",["as"=>"notices.show","uses"=>"NoticeController@show"]);
Route::delete("noticias/{id}",["as"=>"notices.destroy","uses"=>"NoticeController@destroy"]);
Route::get("noticias/{id}/editar",["as"=>"notices.edit","uses"=>"NoticeController@edit"]);
Route::post("noticias",["as"=>"notices.store","uses"=>"NoticeController@store"]);