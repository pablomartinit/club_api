<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TournamentFixtureDate;
use Illuminate\Http\Request;

class TournamentFixtureDateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tournament_fixture_dates = TournamentFixtureDate::orderBy('id', 'desc')->paginate(10);

		return view('tournament_fixture_dates.index', compact('tournament_fixture_dates'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tournament_fixture_dates.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$tournament_fixture_date = new TournamentFixtureDate();

		$tournament_fixture_date->titulo = $request->input("titulo");
        $tournament_fixture_date->tournament_fixture_id = $request->input("tournament_fixture_id");

		$tournament_fixture_date->save();

		return redirect()->route('tournament_fixture_dates.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tournament_fixture_date = TournamentFixtureDate::findOrFail($id);

		return view('tournament_fixture_dates.show', compact('tournament_fixture_date'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tournament_fixture_date = TournamentFixtureDate::findOrFail($id);

		return view('tournament_fixture_dates.edit', compact('tournament_fixture_date'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$tournament_fixture_date = TournamentFixtureDate::findOrFail($id);

		$tournament_fixture_date->titulo = $request->input("titulo");
        $tournament_fixture_date->tournament_fixture_id = $request->input("tournament_fixture_id");

		$tournament_fixture_date->save();

		return redirect()->route('tournament_fixture_dates.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tournament_fixture_date = TournamentFixtureDate::findOrFail($id);
		$tournament_fixture_date->delete();

		return redirect()->route('tournament_fixture_dates.index')->with('message', 'Item deleted successfully.');
	}

}
