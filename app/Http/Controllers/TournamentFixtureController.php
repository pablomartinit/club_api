<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TournamentFixture;
use App\TournamentMatch;
use App\TournamentFixtureDate;
use App\Tournament;
use App\Activity;
use App\Club;
use App\Category;

use Log;

use Illuminate\Http\Request;

class TournamentFixtureController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tournament_fixtures = TournamentFixture::orderBy('id', 'desc')->paginate(10);

		return view('tournament_fixtures.index', compact('tournament_fixtures'));
	}

	/*
	*	Mostramos formulario de fixture
	*/
	public function getForm(){
		$torneos = Tournament::all();
		$actividades = Activity::all();
		$categorias = Category::all();
		$clubes = Club::all();
		return view("tournament_fixtures.completo",compact("torneos","actividades","categorias","clubes"));
	}

	/*
	*	Guardamos Fixture-Jornada-Partido.
	*	
	*	@param Competencia-Categoria/s-Jornada-ClubRival-Partido
	*	@return JSON->OK-ERROR/MSG
	*/
	public function postForm(Request $request){

		$input = $request->all();

		// Revisamos fecha ocupada
		if (  $this->esFechaOcupada($input) ) {
			return response()->json(["response" => [ "conError" => TRUE, "error" => "Fecha ocupada" ] ] );
		}

		$fechaPartidoIngresado = explode("/",$input["fechayhora"]);
		$partidosJugados = TournamentMatch::whereYear('created_at', '=', $fechaPartidoIngresado[0])->get();
		$nuevoFixture = true;
		if ( count($partidosJugados) > 0 ) {
			foreach ($partidosJugados as $key => $partido) {
				$categoria = Category::find($partido->category_id);
				$actividad = Activity::find($categoria->activity_id);
				$torneoPartido = Tournament::where("activities_id",$actividad->id)->get()[0];
				if ( $torneoPartido->id == $input["torneo"] ) {
					$nuevoFixture = false;
					$fixture = TournamentFixture::where("tournament_id",$torneoPartido->id)->get()[0];
				}
			}
		}
		// Fixture nuevo  si no hay partido aun segun el año		
		if ( $nuevoFixture ) {
			$fixture = [ "titulo" => "Fixture ".$fechaPartidoIngresado[0], "tournament_id" => $input["torneo"]  ];
			$id = TournamentFixture::create($fixture)->id;
			$fixture = TournamentFixture::find($id);
			Log::info("Fixture ".$fixture->id." creado!");
		}

		$jornada = new TournamentFixtureDate();
		$jornada->titulo = $input["jornada"];
		$jornada->tournament_fixture_id = $fixture->id;
		$jornada->save();

		if ( $input["local"] ) {
			$input["lugar"] = "Local";
		}

		if ( $input["categoria_actividad_id"] == 0 ) {
			$actividad = Activity::find($input["actividad"]);
			$club_rival = Club::where("nombre",$input["club_rival"])->get()[0];
			foreach ($actividad->category as $key => $cat) {
				$partido = [
					"fechahora" => $input["fechayhora"],
					"lugar" => $input["lugar"],
					"estado" => "1",
					"goles" => "0",
					"goles_rival" => "0",
					"fechahora" => $input["fechayhora"],
					"tournament_fixture_date_id" => $jornada->id,
					"category_id" => $cat->id,
					"club_rival_id" => $club_rival->id
				];
				TournamentMatch::create($partido);
				Log::info("Partido creado");
			}			
		}else{
			$partido = [
				"fechahora" => $input["fechayhora"],
				"lugar" => $input["lugar"],
				"estado" => "1",
				"goles" => "0",
				"goles_rival" => "0",
				"fechahora" => $input["fechayhora"],
				"tournament_fixture_date_id" => $jornada->id,
				"category_id" => $input["categoria_actividad_id"],
				"club_rival_id" => $club_rival->id
			];
			TournamentMatch::create($partido);
			Log::info("Partido creado");
		}

		return response()->json(["response"=> [ "estado" => "exito", "partido" => $input ] ] );
		
	}

	/*
	*	Se revisa fecha ocupada, club ocupado en esa fecha, localia ocupada.
	*
	*/
	public function esFechaOcupada($input){

		if ( $input["categoria_actividad_id"]  == 0 ) {
			$actividad = Activity::find($input["actividad"]);
			$categorias_ids = [];
			foreach ($actividad->category as $key => $value) {
				$categorias_ids[] = $value->id;
			}
		}

		$partidos = TournamentMatch::ocupado_fecha( $input["fechayhora"])
			->whereIn("category_id",$categorias_ids)
			->get();

		if ( count( $partidos ) > 0 ) {
			return true;
		}

	}

	/*
	*	Devuelve el fixture completo de una actividad
	*
	*/
	public function actividad($id){
		// Variables
		$año = date("Y");
		$actividad = Activity::find($id);
		$categorias_ids = [];
		// Conseguimos los ids de categorias en forma de array
		foreach ($actividad->category as $key => $value) {
			$categorias_ids[] = $value->id;
		}

		// Conseguimos los partidos
		$partidos = TournamentMatch::whereYear('fechahora', '=', $año)
						->whereIn("category_id",$categorias_ids)
						->join("clubs","clubs.id","=","tournament_matches.club_rival_id")
						->join("tournament_fixture_dates","tournament_fixture_dates.id","=","tournament_matches.tournament_fixture_date_id")
						->join("categories","categories.id","=","tournament_matches.category_id")
						->select('tournament_matches.*', 'clubs.nombre as club_rival',"tournament_fixture_dates.titulo as jornada","categories.nombre as categoria")
						->orderBy('tournament_matches.fechahora', 'desc')
						->get();

		Log::info($partidos);
		// Conseguimos armar un solo dato para todas las categorias si la jornada es completa
		// $club_rival_const = "";
		// $conteo_categoria = 1;
		// $_partidos = [] ;
		// foreach ($partidos as $key => $partido) {
		// 	if ($partido->club_rival_id == $club_rival_const ) {
		// 		if ( $conteo_categoria == count($categorias_ids) ) {
		// 			$partido->categoria = "Todas las categorias";
		// 			$_partidos[] = $partido;
		// 			$conteo_categoria = 1;	
		// 		}else{
		// 			$conteo_categoria = $conteo_categoria + 1;
		// 		}
		// 	}else{
		// 		$club_rival_const = $partido->club_rival_id;
		// 	}
		// }

		// Log::info($partidos);
		return response()->json(["response"=> [ "estado" => "exito", "partidos" => $partidos ] ] );
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$torneos = Tournament::all();
		return view('tournament_fixtures.create',compact("torneos"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$tournament_fixture = new TournamentFixture();

		$tournament_fixture->titulo = $request->input("titulo");
        		$tournament_fixture->tournament_id = $request->input("tournament_id");

		$tournament_fixture->save();

		return redirect()->route('tournament_fixtures.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tournament_fixture = TournamentFixture::findOrFail($id);

		return view('tournament_fixtures.show', compact('tournament_fixture'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tournament_fixture = TournamentFixture::findOrFail($id);

		return view('tournament_fixtures.edit', compact('tournament_fixture'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$tournament_fixture = TournamentFixture::findOrFail($id);

		$tournament_fixture->titulo = $request->input("titulo");
        $tournament_fixture->tournament_id = $request->input("tournament_id");

		$tournament_fixture->save();

		return redirect()->route('tournament_fixtures.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tournament_fixture = TournamentFixture::findOrFail($id);
		$tournament_fixture->delete();

		return redirect()->route('tournament_fixtures.index')->with('message', 'Item deleted successfully.');
	}

}
