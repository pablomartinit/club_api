<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ActivityGallery;
use App\Activity;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Log;

class ActivityGalleryController extends Controller {


	public function getIndex(){
		$activity_galleries = ActivityGallery::orderBy('id', 'desc')->paginate(10);
		return view('activity_galleries.index',compact("activity_galleries"));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$activity_galleries = ActivityGallery::orderBy('id', 'desc')->paginate(10);
		// return view('activity_galleries.index', compact('activity_galleries'));
		// $activities = Activity::orderBy('id', 'desc')->paginate(10);
		return response()->json(["response"=>compact('activity_galleries')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		$actividades = Activity::all();
		return view('activity_galleries.create',compact("actividades"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->all();
		$activity = Activity::find($input["actividad"]);
		unset($input["actividad"]);
		unset($input["_token"]);
		foreach ($input as $key => $value) {
			$activity_gallery = new ActivityGallery();
			$archivo = $value->getFilename().'.'.$value->getClientOriginalExtension();
			$archivoRuta = public_path()."/actividades/".$activity->id."/galeria/";
			$value->move($archivoRuta,$archivo);     
			$activity_gallery->titulo = "";
			$activity_gallery->url = "";
			$activity_gallery->activity_id = $activity->id;
			$activity_gallery->ruta = $archivoRuta.$archivo;
			$activity_gallery->save();
		}
		// return redirect()->route('activity_galleries.index')->with('message', 'Item created successfully.');
		return response()->json( [ "response"=> "ok!" ] );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$activity_gallery = ActivityGallery::findOrFail($id);

		return view('activity_galleries.show', compact('activity_gallery'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$activity_gallery = ActivityGallery::findOrFail($id);

		return view('activity_galleries.edit', compact('activity_gallery'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$activity_gallery = ActivityGallery::findOrFail($id);

		$activity_gallery->titulo = $request->input("titulo");
		$activity_gallery->url = $request->input("url");
		$activity_gallery->activity_id = $request->input("activity_id");
		$activity_gallery->ruta = $request->input("ruta");

		$activity_gallery->save();

		return redirect()->route('activity_galleries.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$activity_gallery = ActivityGallery::findOrFail($id);
		File::delete($activity_gallery->ruta);
		$activity_gallery->delete();
		return redirect()->route('activity_galleries.index')->with('message', 'Item deleted successfully.');
	}

}
