<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tournament;
use App\Activity;
use Illuminate\Http\Request;

class TournamentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tournaments = Tournament::orderBy('id', 'desc')->paginate(10);

		return view('tournaments.index', compact('tournaments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$actividades = Activity::all();
		return view('tournaments.create',compact("actividades"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$tournament = new Tournament();

		$tournament->nombre = $request->input("nombre");
        $tournament->activities_id = $request->input("activities_id");
        $tournament->descripcion = $request->input("descripcion");
        $tournament->web = $request->input("web");
        $tournament->contacto = $request->input("contacto");

		$tournament->save();

		return redirect()->route('tournaments.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tournament = Tournament::findOrFail($id);

		return view('tournaments.show', compact('tournament'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tournament = Tournament::findOrFail($id);

		return view('tournaments.edit', compact('tournament'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$tournament = Tournament::findOrFail($id);

		$tournament->nombre = $request->input("nombre");
        $tournament->activities_id = $request->input("activities_id");
        $tournament->descripcion = $request->input("descripcion");
        $tournament->web = $request->input("web");
        $tournament->contacto = $request->input("contacto");

		$tournament->save();

		return redirect()->route('tournaments.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tournament = Tournament::findOrFail($id);
		$tournament->delete();

		return redirect()->route('tournaments.index')->with('message', 'Item deleted successfully.');
	}

}
