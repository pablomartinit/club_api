<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Club;
use Illuminate\Http\Request;

class ClubController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$clubs = Club::orderBy('id', 'desc')->paginate(10);
		return view('clubs.index', compact('clubs'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clubs = Club::orderBy('id', 'desc')->paginate(10);
		return response()->json(["response"=>compact('clubs')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('clubs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$club = new Club();

		$club->nombre = $request->input("nombre");
        $club->web = $request->input("web");
        $club->contacto = $request->input("contacto");
        $club->direccion = $request->input("direccion");

		$club->save();

		return redirect()->route('clubs.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$club = Club::findOrFail($id);

		return view('clubs.show', compact('club'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$club = Club::findOrFail($id);

		return view('clubs.edit', compact('club'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$club = Club::findOrFail($id);

		$club->nombre = $request->input("nombre");
        $club->web = $request->input("web");
        $club->contacto = $request->input("contacto");
        $club->direccion = $request->input("direccion");

		$club->save();

		return redirect()->route('clubs.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$club = Club::findOrFail($id);
		$club->delete();

		return redirect()->route('clubs.index')->with('message', 'Item deleted successfully.');
	}

}
