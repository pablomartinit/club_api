<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TournamentMatch;
use Illuminate\Http\Request;

class TournamentMatchController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tournament_matches = TournamentMatch::orderBy('id', 'desc')->paginate(10);

		return view('tournament_matches.index', compact('tournament_matches'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tournament_matches.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$tournament_match = new TournamentMatch();

		$tournament_match->fechahora = $request->input("fechahora");
        $tournament_match->lugar = $request->input("lugar");
        $tournament_match->estado = $request->input("estado");
        $tournament_match->goles = $request->input("goles");
        $tournament_match->goles_rival = $request->input("goles_rival");
        $tournament_match->tournament_fixture_date_id = $request->input("tournament_fixture_date_id");

		$tournament_match->save();

		return redirect()->route('tournament_matches.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tournament_match = TournamentMatch::findOrFail($id);

		return view('tournament_matches.show', compact('tournament_match'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tournament_match = TournamentMatch::findOrFail($id);

		return view('tournament_matches.edit', compact('tournament_match'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$tournament_match = TournamentMatch::findOrFail($id);

		$tournament_match->fechahora = $request->input("fechahora");
        $tournament_match->lugar = $request->input("lugar");
        $tournament_match->estado = $request->input("estado");
        $tournament_match->goles = $request->input("goles");
        $tournament_match->goles_rival = $request->input("goles_rival");
        $tournament_match->tournament_fixture_date_id = $request->input("tournament_fixture_date_id");

		$tournament_match->save();

		return redirect()->route('tournament_matches.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tournament_match = TournamentMatch::findOrFail($id);
		$tournament_match->delete();

		return redirect()->route('tournament_matches.index')->with('message', 'Item deleted successfully.');
	}

}
