<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notices = Notice::orderBy('id', 'desc')->paginate(10);

		return view('notices.index', compact('notices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('notices.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$notice = new Notice();

		$notice->titulo = $request->input("titulo");
        $notice->texto = $request->input("texto");
        $notice->imagen_ruta = $request->input("imagen_ruta");
        $notice->visitas = $request->input("visitas");
        $notice->tag_etiquetas = $request->input("tag_etiquetas");

		$notice->save();

		return redirect()->route('notices.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$notice = Notice::findOrFail($id);

		return view('notices.show', compact('notice'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notice = Notice::findOrFail($id);

		return view('notices.edit', compact('notice'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$notice = Notice::findOrFail($id);

		$notice->titulo = $request->input("titulo");
        $notice->texto = $request->input("texto");
        $notice->imagen_ruta = $request->input("imagen_ruta");
        $notice->visitas = $request->input("visitas");
        $notice->tag_etiquetas = $request->input("tag_etiquetas");

		$notice->save();

		return redirect()->route('notices.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$notice = Notice::findOrFail($id);
		$notice->delete();

		return redirect()->route('notices.index')->with('message', 'Item deleted successfully.');
	}

}
