<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function category(){
    	return $this->hasMany(Category::class);
    }

    public function gallery(){
    	return $this->hasMany(ActivityGallery::class);
    }

    public function tournaments(){
    	return $this->hasMany(Tournament::class);
    }

    public function clubs(){
    	return $this->belongsTo(Club::class);
    }
}
