<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    public function activity(){
    	return $this->belongsTo(Activity::class);
    }

    public function fixture(){
    	return $this->hasMany(TournamentFixture::class);
    }
}
