<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentFixtureDate extends Model
{
    public function fixture(){
    	return $this->belongsTo(TournamentFixture::class);
    }

    public function partido(){
    	return $this->hasMany(TournamentMatch::class);
    }
}
