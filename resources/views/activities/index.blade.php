@extends('layout')

@section('css')

<style type="text/css">
    .row-hidden{
        display: none;
    }
</style>

@endsection

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Actividades
            <!-- <a class="btn btn-success pull-right" href="{{ route('activities.create') }}"><i class="glyphicon glyphicon-plus"></i> Agregar otro</a> -->
            <a class="btn btn-success pull-right" id="boton-form" href="#"><i class="glyphicon glyphicon-plus"></i> Agregar otro</a>
        </h1>

    </div>
@endsection

@section('content')

    <div class="row row-hidden" id="row-form">
        <div class="col-md-12">
            <form method="GET" action="#">

                 <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                 <div class="row">

                     <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">

                         <div class="form-group">
                             <div class="form-line">
                                 <input class="form-control" placeholder="Nombre" id="nombre" type="text">
                             </div>
                         </div>

                     </div>

                     <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">

                         <div class="form-group">
                             <div class="form-line">
                                 <textarea class="form-control" id="descripcion" placeholder="Descripcion..."></textarea>
                             </div>
                         </div>

                     </div>

                     <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                         <button type="button" id="boton-grabar" class="btn btn-primary">GRABAR</button>
                     </div>

                 </div>

             </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th>NOMBRE</th>
                        <th>DESCRIPCION</th>
                    </tr>
                </thead>
                <tbody id="actividad_contenido">
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        botones  = "<a class='btn btn-xs btn-primary' onClick='show(this)' href='#'><i class='glyphicon glyphicon-eye-open' ></i> View</a>"
        botones += "<a class='btn btn-xs btn-warning' onClick='editar(this)' href='#'><i class='glyphicon glyphicon-edit'></i> Edit</a>"
        botones += "<a class='btn btn-xs btn-danger'  onClick='eliminar(this)' href='#'><i class='glyphicon glyphicon-trash'></i> Delete</a>"

        function colocar(data){
            $.each( data.activities.data, function( key, actividad ) {
                tr = "<tr id='"+actividad.id+"'><td>"+actividad.nombre+"</td><td>"+actividad.descripcion+"</td><td class='text-right'>"+botones+"</td></tr>"
                $("#actividad_contenido").append(tr)
            });  
        };

        function eliminar(actividad){
            id = $(actividad).parent().parent()[0].id
            actividad = $("#"+id+" > td").html()
            if ( confirm("Está seguro que desea eliminar: "+actividad+" ?") ) {
                $.ajax({
                    url: "actividades/"+id,
                    method: "delete",
                    data: { _token: $("#_token").val() }
                }).done(function(respuesta){
                    console.log("Eliminado:"+respuesta)
                    $("#"+id).remove()
                }).fail(function(respuesta){
                    console.log("No eliminado:"+respuesta)
                });
            }
        }

        function show(actividad){
            id = $(actividad).parent().parent()[0].id
            window.location = "actividades/"+id
        }

        function editar(actividad){
            id = $(actividad).parent().parent()[0].id
            window.location = "actividades/"+id+"/editar"
        }

        $(document).ready(function(){

            // Al iniciar cargar los datos de la base
            $("#boton-form").click(function(){
                $("#row-form").removeClass("row-hidden");
            })

            $.get('actividades/index')
            .success(function(data) {
                colocar(data.response)
            })
            .error(function(jqXHR, textStatus, errorThrown) {
                console.log("ERROR")     
            });

            $("#boton-grabar").click(function(){
                data = { 
                    nombre: $("#nombre").val(),
                    descripcion: $("#descripcion").val(),
                    _token: $("#_token").val()
                }

                $.post('actividades', data)
                .done(function(data){ 
                    actividad = data.response.activity
                    tr = "<tr id='"+actividad.id+"'><td>"+actividad.nombre+"</td><td>"+actividad.descripcion+"</td><td class='text-right'>"+botones+"</td></tr>"
                    $("#actividad_contenido").append(tr)
                    $("#nombre").val("");
                    $("#descripcion").val("");
                })

                .fail(function(xhr, status, error) {
                    console.log("ERROR")
                });

            });
        })

    </script>


@endsection