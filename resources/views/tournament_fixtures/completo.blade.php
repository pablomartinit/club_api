@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Formulario completo del fixture
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-3">
                    <select tabindex="-98" name="actividad" id="actividad" class="form-control">
                        @foreach( $actividades as $actividad )
                            <option value="{{ $actividad->id }}">{{ $actividad->nombre }}</option>    
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <select tabindex="-98" name="torneo" id="torneo" class="form-control">
                        @foreach( $torneos as $torneo )
                            <option value="{{ $torneo->id }}">{{ $torneo->nombre }}</option>    
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <select tabindex="-98" name="categoria" id="categoria" class="form-control">
                        @foreach( $actividades as $actcat )
                            <option value="0">Todas las categorias de {{ $actcat->nombre }}</option>    
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row ">
                <div class="col-sm-12">
                    <h4>Agregar nuevo</h4>
                    <form>
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" id="jornada" name="jornada" placeholder="Jornada #1" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" id="club_rival" name="club_rival" placeholder="vs..." type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="fechayhora" id="fechayhora" placeholder="Fecha y hora">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" id="lugar" name="lugar" placeholder="direccion..." type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-6">
                                <div class="checkbox">
                                  <label for="local">
                                    <input id="local" checked type="checkbox">
                                    Local
                                  </label>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-6 col-xs-6">
                                <button type="button" id="boton-guardar" class="btn btn-primary btn-lg">GUARDAR</button>
                            </div>
                        </div>
                    </form>
                </div>    
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <h4>Listados</h4>
                    <table style="cursor: pointer;" id="mainTable-2" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Jornada</th>
                                <th>Categoria</th>
                                <th>Club</th>
                                <th>Fecha</th>
                                <th>Lugar</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="fixture_contenido">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script>

        botonEliminar  = "<a class='btn btn-xs btn-primary' onClick='eliminar(this)' href='#'><i class='glyphicon glyphicon-eye-open' ></i> Eliminar</a>"

        function colocar(data){
            $("#fixture_contenido").html("");
            $.each( data.partidos , function( key, partido ) {
                tr = "<tr id='"+partido.id+"'><td>"+partido.jornada+"</td><td>"+partido.categoria+"</td><td>"+partido.club_rival+"</td><td>"+partido.fechahora+"</td><td>"+partido.lugar+"</td><td class='text-right'>"+botonEliminar+"</td></tr>"
                $("#fixture_contenido").append(tr)
            });  
        };

        function getFixture(){
            $.get('{{ url("fixture/actividad") }}' + "/" + $("#actividad").val())
            .success(function(data) {
                // console.log(data.response)
                colocar(data.response)
            })
            .error(function(jqXHR, textStatus, errorThrown) {
                console.log("ERROR")     
            });
        }

        // function eliminar(actividad){
        //     id = $(actividad).parent().parent()[0].id
        //     actividad = $("#"+id+" > td").html()
        //     if ( confirm("Está seguro que desea eliminar: "+actividad+" ?") ) {
        //         $.ajax({
        //             url: "actividades/"+id,
        //             method: "delete",
        //             data: { _token: $("#_token").val() }
        //         }).done(function(respuesta){
        //             console.log("Eliminado:"+respuesta)
        //             $("#"+id).remove()
        //         }).fail(function(respuesta){
        //             console.log("No eliminado:"+respuesta)
        //         });
        //     }
        // }

        // function show(actividad){
        //     id = $(actividad).parent().parent()[0].id
        //     window.location = "actividades/"+id
        // }

        // function editar(actividad){
        //     id = $(actividad).parent().parent()[0].id
        //     window.location = "actividades/"+id+"/editar"
        // }

        $(document).ready(function(){
            getFixture();
            // Al seleccionar una actividad, escribir los partidos
            $("#actividad").change(function(){
                 getFixture();
            })

            // $.get('actividades/index')
            // .success(function(data) {
            //     colocar(data.response)
            // })
            // .error(function(jqXHR, textStatus, errorThrown) {
            //     console.log("ERROR")     
            // });

            $("#boton-guardar").click(function(){


                data = { 
                    actividad: $("#actividad").val(),
                    torneo: $("#torneo").val(),
                    categoria_actividad_id: $("#categoria").val(),
                    jornada: $("#jornada").val(),
                    club_rival: $("#club_rival").val(),
                    fechayhora: $("#fechayhora").val(),
                    lugar: $("#lugar").val(),
                    local: $("#local").val(),
                    _token: $("#_token").val()
                }

                console.log(data);

                $.post( " {{ url('fixture/completo') }} ", data)
                .done(function(data){ 

                    if ( data.response.conError ){
                        alert(data.response.error)
                        return;
                    }

                    getFixture();
                    // tr = "<tr id='"+data.response.partido.id+"'><td>"+data.response.partido.jornada+"</td><td>"+data.response.partido.categoria_actividad_id+"</td><td>"+data.response.partido.club_rival+"</td><td>"+data.response.partido.fechayhora+"</td><td>"+data.response.partido.lugar+"</td><td class='text-right'>"+botonEliminar+"</td></tr>"
                    // $("#fixture_contenido").prepend(tr)
                })
                .fail(function(xhr, status, error) {
                    console.log("ERROR: "+error)
                });

            });
        })

    </script>


@endsection