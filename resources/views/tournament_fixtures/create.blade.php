@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> TournamentFixtures / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('tournament_fixtures.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('titulo')) has-error @endif">
                       <label for="titulo-field">Titulo</label>
                    <input type="text" id="titulo-field" name="titulo" class="form-control" value="{{ old("titulo") }}"/>
                       @if($errors->has("titulo"))
                        <span class="help-block">{{ $errors->first("titulo") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tournament_id')) has-error @endif">
                       <label for="tournament_id-field">Actividad</label>
                        <select name="tournament_id" class="form-control">
                            @foreach( $torneos as $torneo)
                              <option value="{{ $torneo->id }}">{{ $torneo->nombre }}</option>
                            @endforeach
                        </select>
                       @if($errors->has("tournament_id"))
                        <span class="help-block">{{ $errors->first("tournament_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('tournament_fixtures.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
