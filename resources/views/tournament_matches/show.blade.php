@extends('layout')
@section('header')
<div class="page-header">
        <h1>TournamentMatches / Show #{{$tournament_match->id}}</h1>
        <form action="{{ route('tournament_matches.destroy', $tournament_match->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('tournament_matches.edit', $tournament_match->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="fechahora">FECHAHORA</label>
                     <p class="form-control-static">{{$tournament_match->fechahora}}</p>
                </div>
                    <div class="form-group">
                     <label for="lugar">LUGAR</label>
                     <p class="form-control-static">{{$tournament_match->lugar}}</p>
                </div>
                    <div class="form-group">
                     <label for="estado">ESTADO</label>
                     <p class="form-control-static">{{$tournament_match->estado}}</p>
                </div>
                    <div class="form-group">
                     <label for="goles">GOLES</label>
                     <p class="form-control-static">{{$tournament_match->goles}}</p>
                </div>
                    <div class="form-group">
                     <label for="goles_rival">GOLES_RIVAL</label>
                     <p class="form-control-static">{{$tournament_match->goles_rival}}</p>
                </div>
                    <div class="form-group">
                     <label for="tournament_fixture_date_id">TOURNAMENT_FIXTURE_DATE_ID</label>
                     <p class="form-control-static">{{$tournament_match->tournament_fixture_date_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('tournament_matches.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection