@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> TournamentMatches
            <a class="btn btn-success pull-right" href="{{ route('tournament_matches.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($tournament_matches->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>FECHAHORA</th>
                        <th>LUGAR</th>
                        <th>ESTADO</th>
                        <th>GOLES</th>
                        <th>GOLES_RIVAL</th>
                        <th>TOURNAMENT_FIXTURE_DATE_ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($tournament_matches as $tournament_match)
                            <tr>
                                <td>{{$tournament_match->id}}</td>
                                <td>{{$tournament_match->fechahora}}</td>
                    <td>{{$tournament_match->lugar}}</td>
                    <td>{{$tournament_match->estado}}</td>
                    <td>{{$tournament_match->goles}}</td>
                    <td>{{$tournament_match->goles_rival}}</td>
                    <td>{{$tournament_match->tournament_fixture_date_id}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('tournament_matches.show', $tournament_match->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('tournament_matches.edit', $tournament_match->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('tournament_matches.destroy', $tournament_match->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $tournament_matches->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection