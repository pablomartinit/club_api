@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> TournamentMatches / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('tournament_matches.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('fechahora')) has-error @endif">
                       <label for="fechahora-field">Fechahora</label>
                    <input type="text" id="fechahora-field" name="fechahora" class="form-control" value="{{ old("fechahora") }}"/>
                       @if($errors->has("fechahora"))
                        <span class="help-block">{{ $errors->first("fechahora") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('lugar')) has-error @endif">
                       <label for="lugar-field">Lugar</label>
                    <input type="text" id="lugar-field" name="lugar" class="form-control" value="{{ old("lugar") }}"/>
                       @if($errors->has("lugar"))
                        <span class="help-block">{{ $errors->first("lugar") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('estado')) has-error @endif">
                       <label for="estado-field">Estado</label>
                    <input type="text" id="estado-field" name="estado" class="form-control" value="{{ old("estado") }}"/>
                       @if($errors->has("estado"))
                        <span class="help-block">{{ $errors->first("estado") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('goles')) has-error @endif">
                       <label for="goles-field">Goles</label>
                    <input type="text" id="goles-field" name="goles" class="form-control" value="{{ old("goles") }}"/>
                       @if($errors->has("goles"))
                        <span class="help-block">{{ $errors->first("goles") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('goles_rival')) has-error @endif">
                       <label for="goles_rival-field">Goles_rival</label>
                    <input type="text" id="goles_rival-field" name="goles_rival" class="form-control" value="{{ old("goles_rival") }}"/>
                       @if($errors->has("goles_rival"))
                        <span class="help-block">{{ $errors->first("goles_rival") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tournament_fixture_date_id')) has-error @endif">
                       <label for="tournament_fixture_date_id-field">Tournament_fixture_date_id</label>
                    <input type="text" id="tournament_fixture_date_id-field" name="tournament_fixture_date_id" class="form-control" value="{{ old("tournament_fixture_date_id") }}"/>
                       @if($errors->has("tournament_fixture_date_id"))
                        <span class="help-block">{{ $errors->first("tournament_fixture_date_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('tournament_matches.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
