@extends('layout')
@section('header')
<div class="page-header">
        <h1>Notices / Show #{{$notice->id}}</h1>
        <form action="{{ route('notices.destroy', $notice->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('notices.edit', $notice->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="titulo">TITULO</label>
                     <p class="form-control-static">{{$notice->titulo}}</p>
                </div>
                    <div class="form-group">
                     <label for="texto">TEXTO</label>
                     <p class="form-control-static">{{$notice->texto}}</p>
                </div>
                    <div class="form-group">
                     <label for="imagen_ruta">IMAGEN_RUTA</label>
                     <p class="form-control-static">{{$notice->imagen_ruta}}</p>
                </div>
                    <div class="form-group">
                     <label for="visitas">VISITAS</label>
                     <p class="form-control-static">{{$notice->visitas}}</p>
                </div>
                    <div class="form-group">
                     <label for="tag_etiquetas">TAG_ETIQUETAS</label>
                     <p class="form-control-static">{{$notice->tag_etiquetas}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('notices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection