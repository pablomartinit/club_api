@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Notices / Edit #{{$notice->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('notices.update', $notice->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('titulo')) has-error @endif">
                       <label for="titulo-field">Titulo</label>
                    <input type="text" id="titulo-field" name="titulo" class="form-control" value="{{ is_null(old("titulo")) ? $notice->titulo : old("titulo") }}"/>
                       @if($errors->has("titulo"))
                        <span class="help-block">{{ $errors->first("titulo") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('texto')) has-error @endif">
                       <label for="texto-field">Texto</label>
                    <input type="text" id="texto-field" name="texto" class="form-control" value="{{ is_null(old("texto")) ? $notice->texto : old("texto") }}"/>
                       @if($errors->has("texto"))
                        <span class="help-block">{{ $errors->first("texto") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('imagen_ruta')) has-error @endif">
                       <label for="imagen_ruta-field">Imagen_ruta</label>
                    <input type="text" id="imagen_ruta-field" name="imagen_ruta" class="form-control" value="{{ is_null(old("imagen_ruta")) ? $notice->imagen_ruta : old("imagen_ruta") }}"/>
                       @if($errors->has("imagen_ruta"))
                        <span class="help-block">{{ $errors->first("imagen_ruta") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('visitas')) has-error @endif">
                       <label for="visitas-field">Visitas</label>
                    <input type="text" id="visitas-field" name="visitas" class="form-control" value="{{ is_null(old("visitas")) ? $notice->visitas : old("visitas") }}"/>
                       @if($errors->has("visitas"))
                        <span class="help-block">{{ $errors->first("visitas") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('tag_etiquetas')) has-error @endif">
                       <label for="tag_etiquetas-field">Tag_etiquetas</label>
                    <input type="text" id="tag_etiquetas-field" name="tag_etiquetas" class="form-control" value="{{ is_null(old("tag_etiquetas")) ? $notice->tag_etiquetas : old("tag_etiquetas") }}"/>
                       @if($errors->has("tag_etiquetas"))
                        <span class="help-block">{{ $errors->first("tag_etiquetas") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('notices.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
