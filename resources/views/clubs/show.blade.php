@extends('layout')
@section('header')
<div class="page-header">
        <h1>Clubs / Show #{{$club->id}}</h1>
        <form action="{{ route('clubs.destroy', $club->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('clubs.edit', $club->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="nombre">NOMBRE</label>
                     <p class="form-control-static">{{$club->nombre}}</p>
                </div>
                    <div class="form-group">
                     <label for="web">WEB</label>
                     <p class="form-control-static">{{$club->web}}</p>
                </div>
                    <div class="form-group">
                     <label for="contacto">CONTACTO</label>
                     <p class="form-control-static">{{$club->contacto}}</p>
                </div>
                    <div class="form-group">
                     <label for="direccion">DIRECCION</label>
                     <p class="form-control-static">{{$club->direccion}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('clubs.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection