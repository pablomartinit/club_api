@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Clubs / Edit #{{$club->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('clubs.update', $club->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('nombre')) has-error @endif">
                       <label for="nombre-field">Nombre</label>
                    <input type="text" id="nombre-field" name="nombre" class="form-control" value="{{ $club->nombre }}"/>
                       @if($errors->has("nombre"))
                        <span class="help-block">{{ $errors->first("nombre") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('web')) has-error @endif">
                       <label for="web-field">Web</label>
                    <input type="text" id="web-field" name="web" class="form-control" value="{{ $club->web }}"/>
                       @if($errors->has("web"))
                        <span class="help-block">{{ $errors->first("web") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('contacto')) has-error @endif">
                       <label for="contacto-field">Contacto</label>
                    <input type="text" id="contacto-field" name="contacto" class="form-control" value="{{ $club->contacto }}"/>
                       @if($errors->has("contacto"))
                        <span class="help-block">{{ $errors->first("contacto") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('direccion')) has-error @endif">
                       <label for="direccion-field">Direccion</label>
                    <input type="text" id="direccion-field" name="direccion" class="form-control" value="{{ $club->direccion }}"/>
                       @if($errors->has("direccion"))
                        <span class="help-block">{{ $errors->first("direccion") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('clubs.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
