@extends('layout')
@section('header')
<div class="page-header">
        <h1>ActivityGalleries / Show #{{$activity_gallery->id}}</h1>
        <form action="{{ route('activity_galleries.destroy', $activity_gallery->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('activity_galleries.edit', $activity_gallery->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="titulo">TITULO</label>
                     <p class="form-control-static">{{$activity_gallery->titulo}}</p>
                </div>
                    <div class="form-group">
                     <label for="url">URL</label>
                     <p class="form-control-static">{{$activity_gallery->url}}</p>
                </div>
                    <div class="form-group">
                     <label for="activity_id">ACTIVITY_ID</label>
                     <p class="form-control-static">{{$activity_gallery->activity_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="ruta">RUTA</label>
                     <p class="form-control-static">{{$activity_gallery->ruta}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('activity_galleries.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection