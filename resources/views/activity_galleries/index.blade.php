@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> ActivityGalleries
            <a class="btn btn-success pull-right" href="{{ route('activity_galleries.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($activity_galleries->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITULO</th>
                            <th>URL</th>
                            <th>ACTIVITY_ID</th>
                            <th>RUTA</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($activity_galleries as $activity_gallery)
                            <tr>
                                <td>{{$activity_gallery->id}}</td>
                                <td>{{$activity_gallery->titulo}}</td>
                                <td>{{$activity_gallery->url}}</td>
                                <td>{{$activity_gallery->activity_id}}</td>
                                <td>{{$activity_gallery->ruta}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('activity_galleries.show', $activity_gallery->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('activity_galleries.edit', $activity_gallery->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('activity_galleries.destroy', $activity_gallery->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $activity_galleries->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection