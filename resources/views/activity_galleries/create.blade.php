@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> ActivityGalleries / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('activity_galleries.store') }}" id="form-galeria" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                 <div class="form-group @if($errors->has('activity_id')) has-error @endif">
                       <label for="activity_id-field">Actividad</label>
                        <select name="activity_id" id="activity_id" class="form-control">
                            @foreach( $actividades as $actividad)
                              <option value="{{ $actividad->id }}">{{ $actividad->nombre }}</option>
                            @endforeach
                        </select>
                       @if($errors->has("activity_id"))
                        <span class="help-block">{{ $errors->first("activity_id") }}</span>
                       @endif
                 </div>

                 <div class="form-group @if($errors->has('galeria')) has-error @endif">
                       <label for="galeria-field">Galeria</label>
                       <input type="file" name="galeria[]" id="galeria" multiple="multiple">
                       @if($errors->has("galeria"))
                        <span class="help-block">{{ $errors->first("galeria") }}</span>
                       @endif
                 </div>



                  <div class="form-group @if($errors->has('url')) has-error @endif">
                     <label for="url-field">Url</label>
                  <input type="text" id="url-field" name="url" class="form-control" value="{{ old("url") }}"/>
                     @if($errors->has("url"))
                      <span class="help-block">{{ $errors->first("url") }}</span>
                     @endif
                  </div>

                <div class="well well-sm">
                    <!-- <button type="submit" class="btn btn-primary">Create</button> -->
                    <button type="button" id="boton-grabar" class="btn btn-primary">Grabar</button>
                    <a class="btn btn-link pull-right" href="{{ route('activity_galleries.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
        
        <script>

  //       	botones  = "<a class='btn btn-xs btn-primary' onClick='show(this)' href='#'><i class='glyphicon glyphicon-eye-open' ></i> View</a>"
	 //        botones += "<a class='btn btn-xs btn-warning' onClick='editar(this)' href='#'><i class='glyphicon glyphicon-edit'></i> Edit</a>"
  //       	botones += "<a class='btn btn-xs btn-danger'  onClick='eliminar(this)' href='#'><i class='glyphicon glyphicon-trash'></i> Delete</a>"

		// function colocar(data){
		//     $.each( data.activities.data, function( key, actividad ) {
		//         tr = "<tr id='"+actividad.id+"'><td>"+actividad.nombre+"</td><td>"+actividad.descripcion+"</td><td class='text-right'>"+botones+"</td></tr>"
		//         $("#actividad_contenido").append(tr)
		//     });
		// };

		// function eliminar(actividad){
		//     id = $(actividad).parent().parent()[0].id
		//     actividad = $("#"+id+" > td").html()
		//     if ( confirm("Está seguro que desea eliminar: "+actividad+" ?") ) {
		//         $.ajax({
		//             url: "actividades/"+id,
		//             method: "delete",
		//             data: { _token: $("#_token").val() }
		//         }).done(function(respuesta){
		//             console.log("Eliminado:"+respuesta)
		//             $("#"+id).remove()
		//         }).fail(function(respuesta){
		//             console.log("No eliminado:"+respuesta)
		//         });
		//     }
		// }

		// function show(actividad){
		//     id = $(actividad).parent().parent()[0].id
		//     window.location = "actividades/"+id
		// }

		// function editar(actividad){
		//     id = $(actividad).parent().parent()[0].id
		//     window.location = "actividades/"+id+"/editar"
		// }

		$(document).ready(function(){

		    // Al iniciar cargar los datos de la base
		    // $("#boton-form").click(function(){
		    //     $("#row-form").removeClass("row-hidden");
		    // })

		    // $.get('actividades/index')
		    // .success(function(data) {
		    //     colocar(data.response)
		    // })
		    // .error(function(jqXHR, textStatus, errorThrown) {
		    //     console.log("ERROR")
		    // });

		    $("#boton-grabar").click(function(){
                      var galeria = $("#galeria")[0];                      
                      data = new FormData();
                      data.append('actividad', $("#activity_id").val());
                      data.append('_token', $("#_token").val());
                      $.each(galeria.files, function(key, value)
                      {
                          data.append("archivo_"+key, value);
                      });

                      $.ajax({
                          async: false,
                          url: '{{ url("actividad/galeria") }} ',
                          type: 'POST',
                          contentType: false,
                          data: data,
                          processData: false,
                          cache: false,
                          success: function(resp){
                            window.location.href = '{{ url("actividad/galeria") }}'
                          },
                          beforeSend: function(){
                            console.log("enviando...")
                          },
                          fail: function(resp){
                            alert(resp)
                          },
                      });
		    });
		})

    </script>

@endsection
