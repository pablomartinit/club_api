@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> ActivityGalleries / Edit #{{$activity_gallery->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('activity_galleries.update', $activity_gallery->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('titulo')) has-error @endif">
                       <label for="titulo-field">Titulo</label>
                    <input type="text" id="titulo-field" name="titulo" class="form-control" value="{{ is_null(old("titulo")) ? $activity_gallery->titulo : old("titulo") }}"/>
                       @if($errors->has("titulo"))
                        <span class="help-block">{{ $errors->first("titulo") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('url')) has-error @endif">
                       <label for="url-field">Url</label>
                    <input type="text" id="url-field" name="url" class="form-control" value="{{ is_null(old("url")) ? $activity_gallery->url : old("url") }}"/>
                       @if($errors->has("url"))
                        <span class="help-block">{{ $errors->first("url") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('activity_id')) has-error @endif">
                       <label for="activity_id-field">Activity_id</label>
                    <input type="text" id="activity_id-field" name="activity_id" class="form-control" value="{{ is_null(old("activity_id")) ? $activity_gallery->activity_id : old("activity_id") }}"/>
                       @if($errors->has("activity_id"))
                        <span class="help-block">{{ $errors->first("activity_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('ruta')) has-error @endif">
                       <label for="ruta-field">Ruta</label>
                    <input type="text" id="ruta-field" name="ruta" class="form-control" value="{{ is_null(old("ruta")) ? $activity_gallery->ruta : old("ruta") }}"/>
                       @if($errors->has("ruta"))
                        <span class="help-block">{{ $errors->first("ruta") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('activity_galleries.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
