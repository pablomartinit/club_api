@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Tournaments / Edit #{{$tournament->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('tournaments.update', $tournament->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('nombre')) has-error @endif">
                       <label for="nombre-field">Nombre</label>
                    <input type="text" id="nombre-field" name="nombre" class="form-control" value="{{ $tournament->nombre }}"/>
                       @if($errors->has("nombre"))
                        <span class="help-block">{{ $errors->first("nombre") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('activities_id')) has-error @endif">
                       <label for="activities_id-field">Activities_id</label>
                    <input type="text" id="activities_id-field" name="activities_id" class="form-control" value="{{ $tournament->activities_id }}"/>
                       @if($errors->has("activities_id"))
                        <span class="help-block">{{ $errors->first("activities_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('descripcion')) has-error @endif">
                       <label for="descripcion-field">Descripcion</label>
                    <input type="text" id="descripcion-field" name="descripcion" class="form-control" value="{{ $tournament->descripcion }}"/>
                       @if($errors->has("descripcion"))
                        <span class="help-block">{{ $errors->first("descripcion") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('web')) has-error @endif">
                       <label for="web-field">Web</label>
                    <input type="text" id="web-field" name="web" class="form-control" value="{{ $tournament->web }}"/>
                       @if($errors->has("web"))
                        <span class="help-block">{{ $errors->first("web") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('contacto')) has-error @endif">
                       <label for="contacto-field">Contacto</label>
                    <input type="text" id="contacto-field" name="contacto" class="form-control" value="{{ $tournament->contacto }}"/>
                       @if($errors->has("contacto"))
                        <span class="help-block">{{ $errors->first("contacto") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('tournaments.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
