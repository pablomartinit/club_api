@extends('layout')
@section('header')
<div class="page-header">
        <h1>TournamentFixtureDates / Show #{{$tournament_fixture_date->id}}</h1>
        <form action="{{ route('tournament_fixture_dates.destroy', $tournament_fixture_date->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('tournament_fixture_dates.edit', $tournament_fixture_date->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="titulo">TITULO</label>
                     <p class="form-control-static">{{$tournament_fixture_date->titulo}}</p>
                </div>
                    <div class="form-group">
                     <label for="tournament_fixture_id">TOURNAMENT_FIXTURE_ID</label>
                     <p class="form-control-static">{{$tournament_fixture_date->tournament_fixture_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('tournament_fixture_dates.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection