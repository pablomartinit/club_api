# Club Admin Web

Se propone desarrollar, diseñar, programar, y gestionar el sitio web para clubes mejor preparado.
La información será cargada desde un panel administrador gestionado por distintos usuarios o uno solo. Esto se tendrá que acordar.


![club.png](https://bitbucket.org/repo/raaLKr/images/2575367505-club.png)


![actividades.png](https://bitbucket.org/repo/raaLKr/images/3253096722-actividades.png)

## Principalmente preparado para:

Actividades
Categorias de esas actividades
Competencias de la activadad
Seguimiento jornada a jornada de cada actividad
Blog y noticias para compartir
Trofeos/Logros de cada actividad
Multimedia de fotos y videos
Tienda preparada para el orden, reserva y compras
La historia del club y demás informativos se realizan de manera estatica para enfocar mas el estilo de cada club.


## Actividades

Concentran todas las categorias, las competiciones, los logros, las jornadas, los rivales, etc. Se encarga de armar el seguimiento de cada actividad en cada uno de sus lados, toda la información necesaria para estar informado esta acá.


![resultados_web.png](https://bitbucket.org/repo/raaLKr/images/2191520098-resultados_web.png)


Cada actividad tendrá su ABM. Ej: Futbol infantil, Futsal, Basquet, Hockey, Futbol 11, Boxeo.
Cada categoria corresponde a una actividad: Ej: Categoria 90 (Futbol infantil), Primera (futsal), Cadetes(Futsal)
Cada competicion tendrá su ABM, son aquellas competencias a las que pertenece la actividad, puede ser torneo largo o copa corta: Ej: Liga Metropolitana de futsal, F.A.F.I.
Las competiciones tienen rivales (equipos o personas) y están ligados a un fixture o grupo de jornadas. Cada categoria de cada actividad está ligada a un fixture que enfrenta a estos rivales.
Cada competencia cuando termina tiene un trofeo o logro, que es cargado si se consigue.

## Tienda

La tienda virtual como la conocemos hoy en dia, enfocando el estilo y cada necesidad al club.

## Blog y noticias

Un sistema de blog y noticias como cualquier otro, que permita compartir con redes sociales.

## +Trabajos
- Administracion de redes sociales
- Gestion de socios.