<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentMatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_matches', function(Blueprint $table) {
            		$table->increments('id');
            		$table->timestamp('fechahora');
            		$table->string('lugar');
            		$table->integer('estado');
            		$table->integer('goles');
            		$table->integer('goles_rival');
            		$table->integer('tournament_fixture_date_id');
            		$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_matches');
	}

}
